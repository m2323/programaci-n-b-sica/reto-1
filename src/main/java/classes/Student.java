package classes;

/**
 * This class contains the attributes and methods to create a student object.  
 * @author jcebalus
 */
public class Student {
    private final int id;
    private final String name;
    private final String last_name;
    private final String subject;
    private String state = "Pending to evaluate.";
    private double score_1;
    private double score_2;
    private double average;

   
    /**
     * Parameterized constructor 
     * @param id student's identiofication
     * @param name student's name
     * @param last_name student's last name
     * @param subject subject the student is studying
     */
    public Student(int id, String name, String last_name, String subject) {
        this.id = id;
        this.name = name;
        this.last_name = last_name;
        this.subject = subject;
    }

    /**
     * This method return state of the subject
     * @return 
     */
    public String getState() {
        return state;
    }

    /**
     * This method return the first score of subject
     * @return 
     */
    public double getScore_1() {
        return score_1;
    }
    /**
     * This method establish the first score of subject
     * @param score_1 
     */
    public void setScore_1(double score_1) {
        this.score_1 = score_1;
    }
    /**
     * This method return the second score of subject
     * @return 
     */
    public double getScore_2() {
        return score_2;
    }
    /**
     * This method establish the second score of subject
     * @param score_2 
     */
    public void setScore_2(double score_2) {
        this.score_2 = score_2;
    }
    /**
     * This method return the average of subject
     * @return 
     */
    public double getAverage() {
        return average;
    }
    /**
     * Calculates the average between two grades in a subject and define the state
     */
    public void calculateAverage() {
        this.average = (this.score_1 + this.score_2) / (2.0);
        if (this.average <= 2.6 ) {
            this.state = "Reprobate.";
        } else if (this.average >= 2.7 && this.average <= 2.9){
            this.state = "Pending revaluation.";
        } else if (average >= 3.0) {
            this.state = "Approved.";
        }
    }
    /**
     * This method establish the form to print a student
     * @return student
     */
    @Override
    public String toString() {
        return "Name: " + name + " " + last_name + "\tSubject: " + subject + "\tState: " + state;
    }
    
}
