/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Scanner;

/**
 * This class implements a simple menu
 * @author jcebalus
 */
public class Menu {
    
    Scanner scan = new Scanner(System.in);
    /**
     * the main menu of de program
     * @return the menu option chosen by the user 
     */
    public int starMenu(){
        System.out.println("\n");
        System.out.println("Students Management System");
        System.out.println();
        System.out.println("1. Add student.");
        System.out.println("2. Calculate stundet average.");
        System.out.println("3. Show stundet.");
        System.out.println("4. Delete student.");
        System.out.println("5. Show all students.");
        System.out.println("0. Exit.");
        System.out.println();
        System.out.println("Choice a option: ");
        
        return scan.nextInt();
    }
    
}
