
package classes;
import java.util.*;
import classes.Student;
/**
 * This class contains the form to save the information of students
 * @author jcebalus
 */
public class Maps {
    
    HashMap<String, Student> students = new HashMap<String, Student>();
    /**
     * Add a new student to the map
     * @param id
     * @param student 
     */
    public void addStudent(int id, Student student){
        students.put(Integer.toString(id), student);
    }
    /**
     * Find a studet to the map related to id
     * @param id
     * @return object student
     */
    public Student getStudent(int id){
        return students.get(Integer.toString(id));
    }
    /**
     * Delete a student by id
     * @param id 
     */
    public void deleteStudent(int id){
        students.remove(Integer.toString(id));
    }
    /**
     * Shows all the students that the map contains
     */
    public void showAllStudents(){
        if(students.size() == 0){
            System.out.println("No students logged");
        } else {
            for(Map.Entry<String, Student> student: students.entrySet()){
                Student value = student.getValue();
                System.out.println(value);
            }
        }
    }
}
            
    
    
