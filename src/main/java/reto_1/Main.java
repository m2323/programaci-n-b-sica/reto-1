    
package reto_1;
import classes.Student;
import classes.Maps;
import classes.Menu;
import java.util.Scanner;

/**
 * Program that implements a simple student management system.
 * In this program you can add and remove students.
 * It also allows to establish the average of a course with two grades, to validate if the student passed, failed or should be re-evaluated. 
 * @author jcebalus
 * @version 1.0
 * @see classes.Student classes.Maps classes.Menu
 */
public class Main {
    public static void main(String[] args) {
        //Initialize variables 
        int id;
        String name;
        String last_name;
        String subject;
        int opc;
        boolean m = true;
        Scanner scanner = new Scanner(System.in);
        Maps map = new Maps();
        Menu menu = new Menu();
        Student student;
        //Infinity loop
        while(m) {
            opc = menu.starMenu();
            
            switch (opc){
                case 0 -> {
                    System.out.println();
                    System.out.println("EXIT");
                    System.out.println();
                    m = false;
                }
                case 1 -> {
                    System.out.println();
                    System.out.println("ADD STUDENT");
                    System.out.println();
                    
                    System.out.println("Identificaiton:");
                    id = scanner.nextInt();

                    System.out.println("First name:");
                    name = scanner.next();

                    System.out.println("Last name:");
                    last_name = scanner.next();

                    System.out.println("Subject:");
                    subject = scanner.next();                    
                    map.addStudent(id, new Student(id, name, last_name, subject));
                    System.out.println("Added student successfully");
                    System.out.println();
                    System.out.println("Insert 'next' to continue...");
                    scanner.next();
                }
                case 2 -> {
                    System.out.println();
                    System.out.println("CALCULATE STUDENT AVERAGE");
                    System.out.println();
                    System.out.println("Insert student identification:");
                    id = scanner.nextInt();
                    student = map.getStudent(id);
                    if (student==null){
                        System.out.println("Student no found.");
                    } else {
                        System.out.println("Enter first score:");
                        student.setScore_1(scanner.nextDouble());
                        System.out.println("Enter second score:");
                        student.setScore_2(scanner.nextDouble());
                        System.out.println("Calculate average...");
                        student.calculateAverage();
                        System.out.println("Average: "+student.getAverage()+"\tState: "+student.getState());
                    }
                    System.out.println();
                    System.out.println("Insert 'next' to continue...");
                    scanner.next();
                }
                case 3 -> {
                    System.out.println();
                    System.out.println("SHOW STUDETN");
                    System.out.println();
                    System.out.println("Insert student identification:");
                    id = scanner.nextInt();
                    student = map.getStudent(id);
                    if (student==null){
                        System.out.println("Student no found.");
                    } else {
                        System.out.println(student);
                    }
                    System.out.println();
                    System.out.println("Insert 'next' to continue...");
                    scanner.next();
                }
                case 4 -> {
                    System.out.println();
                    System.out.println("DELETE STUDENT");
                    System.out.println();
                    System.out.println("Insert student identification:");
                    id = scanner.nextInt();
                    student = map.getStudent(id);
                    if (student==null){
                        System.out.println("Student no found.");
                    } else {
                        map.deleteStudent(id);
                        System.out.println("Deleted student successfully");
                    }
                    System.out.println();
                    System.out.println("Insert 'next' to continue...");
                    scanner.next();
                }
                case 5 -> {
                    System.out.println();
                    System.out.println("SHOW ALL STUDENTS");
                    System.out.println();
                    map.showAllStudents();
                    System.out.println();
                    System.out.println("Insert 'next' to continue...");
                    scanner.next();
                }
            }   
        }  
    }
}
